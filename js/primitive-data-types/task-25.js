(function() {
  let name = prompt('Введите ваше имя');
  let surname = prompt('Введите вашу фамилию');
  let patronymic = prompt('Введите ваше отчество');
  let personalInfo = surname + name + patronymic;

  let age = prompt('Введите ваш возраст');
  let sex = confirm('Ваш пол мужской?');
  if (sex === true) {
    sex = 'мужской';
  } else {
    sex = 'женский';
  }
  // sex === true ? 'мужской' : 'женский'; 

  let pension;
  if (age < 60) {
    pension = 'нет';
  } else {
    pension = 'да';
  }
  // age < 60 ? 'нет' : 'да';

  alert(`Ваше ФИО:${personalInfo}\nВаш возраст в годах:${+age}\nВаш возраст в днях:${+age * 12 * 30}\nЧерез 5 лет вам будет:${+age + 5}\nВаш пол:${sex}\nВы на пенсии:${pension}`);
})();