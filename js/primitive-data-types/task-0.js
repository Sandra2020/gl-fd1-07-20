(function() {
//string
let boysHolidays = 'Spain';
let campTent = 'fun';
let keptBumpThings = 'in the dark';
let clownChempanzee = 'did tricks';
let MilkForChildTeeth = 'good';
let trampOnFlowers = 'musn\'t';
let frogLife = 'jumps and swims';
let crabInCrack = 'in the rock';
let windShapTwigs = 'i hear';
let dripDropsSink = 'from the tap';

//object
let car = {
  color: 'blue',
  doorCount: 4,
  type: 'sedan',
  isForSale: false,
  number: 1
};

let dog = {
  color: 'grey',
  number: 1,
  pawCount: 4,
  isAngry: false,
  isDomestic: true
};

let hat = {
  model: 'child',
  number: 1,
  color: 'blue',
  isDecorPatterns: true
};

let paint = {
  color: orange,
  number: 1,
  packaching: 'bank'
};

let pirate = {
  isHasHat: true,
  isHavePet: 'parrot',
  age: 50,
  eyesCount: 1,
  isHairOnHead: false
};

let bucket = {
  color: 'green',
  valumeL: 10,
  isHasHandle: true,
  isFilledWater: false
};

let croynos = {
  number: 5,
  color: 'multicolored',
  isPackaching: true,
  packachinColor: 'orange'
};

let pumpkin = {
  color: 'orange',
  isFruit: false,
  isVegetable: true,
  number: 1
};

let rabbit = {
  color: 'lightbrown',
  earsCount: 2,
  IsJump: true
};

let banana = {
  color: 'yellow',
  number: 1,
  isFresh: true
};

let camera = {
  model: 'twinkl',
  isBrocken: false,
  pxCount: 20,
  color: 'black',
  isExpensive: true
};

let dinosaur = {
  isStillAlive: false,
  isAngry: true,
  sizeM: 20,
  age: 230000000
};

let elephant = {
  color: 'grey',
  trunkCount: 1,
  isBig: true
};

let umbrella = {
  color:'yellow',
  mechanism: 'automatic',
  copachtness: true,
  number: 1
};

let alligator = {
  color: 'green',
  isKind: false,
  teethCount: 76,
  isPet: false
};

let helicopter = {
  color: 'red',
  isMoves: false,
  isExpensive: true
};

let television = {
  color: 'black',
  dmCount: 50,
  cost$: 1000,
  isNessesary: false
};

let watermelon = {
  isTasty: true,
  count: 1,
  weightKg: 5,
  color: 'green',
  isRotten: false
};

//number 

let iceCreamsCount = 1;
let shipsCount = 2;
let shortsCount = 3;
let chamomilesCount = 4;
let butterfliesCount = 5;
let carsCount = 6;
let fishCount = 7;
let dinosaursCount = 8;
let mouseCount = 9;
let applesCount = 10;

//boolean

let isSupportGood = true;
let isGreedyGood = false;
let isHaveBreakfastGood = true;
let isPityGood = true;
let isShoveBad = true;
let isShoutBad = true;
let isJumpRopeBad = false;
let isPlayGoos = true;
let isPinchGood = false;
let isReadBad = false;
let isBiteBad = true;
let isPlayBallGood = true;
let isNotShareGood = false;
let isBeGallantGood = true;
let isFightGood = false;
let isTakeFiveGood = true;
let isTripUpGood = false;
let isHelpSbodyGood = true;
let isFightBad = true;
let isPlayFootballGood  = true;
let isWalkBad = false;

})()