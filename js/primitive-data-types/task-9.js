(function() {
    let month = 8;

    if (month === 12 || month <=2) {
        console.log('winter');
    } else if (month >= 3 && month <= 5) {
        console.log('spring');
    } else if (month >= 6 && month <= 8) {
        console.log('summer');
    } else {
        console.log('autumn');
    }
})();